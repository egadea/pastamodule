<?php

/**
 * <ModuleClassName> => Pasta
 * <FileName> => algo.php
 * Format expected: <ModuleClassName><FileName>ModuleFrontController
 * https://itaupy.local:10443/index.php?fc=module&module=PastaModule&controller=Algo
 */
require_once __DIR__.'/../../vendor/autoload.php';
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;

use Lcobucci\JWT\Token\Signature;
use Lcobucci\JWT\Validation\ConstraintViolation;
use Lcobucci\JWT\Validation\Constraint\SignedWith;
use Lcobucci\JWT\Parser;

class PastaModuleAlgoModuleFrontController extends ModuleFrontController
{
    public $ssl = true;

    public function initContent(){
        parent::initContent();

        $user_id = "256050";
        $user_external_id = "1234567";

        $exp = 3600;

        $user = array('user_id'=>$user_id,'external_user_id'=>$user_external_id);
        //$context = array("user"=>$user,"group"=>" * ");
        $signer = new Sha256();
        $admin_token = (new Builder())->set('user_id',$user_id)
                                    ->set('external_user_id',$user_external_id)
                                    //->setExpiration(time() + $exp)
                                    ->setHeader("alg","HS256")
                                    ->sign($signer, 't!3nD@N@R@nJ@m0nCh!s')
                                    ->getToken();

        dump(($admin_token));


        $token = (new Parser())->parse('eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoiMjU2MDUwIiwiZXh0ZXJuYWxfdXNlcl9pZCI6IjEyMzQ1NjcifQ.p7itXD0Vq8vYEPJI_zjzrgtNdP3GZUEEmm9pwD-ouas');
        
        dump('validacion', $token->verify($signer,'t!3nD@N@R@nJ@m0nCh!s'));
    }
}


/**
 * 
 * Tabla Token (client_id,secret_key,product_id)
 * 
 * 
 */
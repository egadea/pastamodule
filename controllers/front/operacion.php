<?php

require_once __DIR__ . '/../AbstractRestController.php';
require_once __DIR__.'/../../vendor/autoload.php';
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;

use Lcobucci\JWT\Token\Signature;
use Lcobucci\JWT\Validation\ConstraintViolation;
use Lcobucci\JWT\Validation\Constraint\SignedWith;
use Lcobucci\JWT\Parser;

class PastaModuleOperacionModuleFrontController extends AbstractRestController
{
    /**
     * tabla Client_token
     * id,client_id, client_secret, product_id, url
     */

     /**
     * tabla token_request
     * id,user_id,cart_id, token, id_specific_price,price,estado,id_client_token
     */


    protected function processGetRequest()
    {
        //Recibo id producto
        $product_id = Tools::getValue ('product_id');
        
        //Busco client y secret
        $db = \Db::getInstance();
        $request = 'SELECT * FROM `' . _DB_PREFIX_ . 'pasta_clients` where product_id ='.$product_id;
      
        $result = $db->getRow($request);
        if ($result == false){
            $this->ajaxDie(json_encode([
                'success' => false,
                'operation' => 'get',
                'error'=>'La entidad con este id no existe'
            ]));
        }
        
        //generar token
        $token  = $this->generarJwt($this->context->customer->id,$result['client_secret']);
        // 't!3nD@N@R@nJ@m0nCh!s'
        //$this->context->customer->id;

        //Guardar Token
        $db->insert('pasta_requests', [
            'user_id' => (int) $this->context->customer->id,
            'token' => pSQL($token->toString()),
            'fecha_creacion' => date('Y-m-d H:i:s'),
        ]);

        //return redirect con token
        $this->ajaxDie(json_encode([
            'token'=>$token->toString(),
            'success' => true,
            'operation' => 'get'
        ]));
    }

    protected function processPostRequest()
    {

        //recibo data con token
        $datos = Tools::getValue ('datos');
        //Busco Token DB
        $db = \Db::getInstance();
        $requestSql = 'SELECT * FROM `' . _DB_PREFIX_ . 'pasta_requests` where token ='.$datos->token;
        $token_req = $db->getRow($requestSql);
        
        $clientSql = 'SELECT * FROM `' . _DB_PREFIX_ . 'pasta_clients` where id ='.$token_req->id_client_token;
        $client_req = $db->getRow($clientSql);
        //valido token


        //genero specific price
        $this->generarSpecificPrice($clientSql->product_id);

        //genero url con hash  y grabo en tbl request ?

        //return ok y url
        
        $this->ajaxDie(json_encode([
            'url'=>'http..../order?time='.time(),
            'success' => true,
            'operation' => 'post'
        ]));
    }

    protected function processPutRequest()
    {
        // recibo token y data
        // valido token
        // busco en tbl_request
        // uupdate status
        // return OK
        $this->ajaxDie(json_encode([
            'success' => true,
            'operation' => 'put'
        ]));
    }

    protected function processDeleteRequest()
    {
        // recibo token y data
        // valido token
        // busco en tbl_request
        // borro
        // return OK
        $this->ajaxDie(json_encode([
            'success' => true,
            'operation' => 'delete'
        ]));
    }


    private function generarJwt($user_id,$secret){
        //$user_id = "256050";
        //$user_external_id = "1234567";

        $exp = 3600;

        //$context = array("user"=>$user,"group"=>" * ");
        $signer = new Sha256();
        $admin_token = (new Builder())->set('user_id',$user_id)
                                    //->set('external_user_id',$user_external_id)
                                    //->setExpiration(time() + $exp)
                                    ->setHeader("alg","HS256")
                                    ->sign($signer, $secret)
                                    ->getToken();

        return($admin_token);
    }

    private function verifyJwt($key,$secret){
        $token = (new Parser())->parse('eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoiMjU2MDUwIiwiZXh0ZXJuYWxfdXNlcl9pZCI6IjEyMzQ1NjcifQ.p7itXD0Vq8vYEPJI_zjzrgtNdP3GZUEEmm9pwD-ouas');
        
        //dump('validacion', $token->verify($signer,'t!3nD@N@R@nJ@m0nCh!s'));
        return $token->verify($signer,'t!3nD@N@R@nJ@m0nCh!s');
    }

    private function generarSpecificPrice($product_id){
        //$product_id = 51807;

        if (is_null($this->context->cart)) {
            $this->context->cart = new Cart($this->context->cookie->id_cart);
        }

        if (is_null($this->context->cart->id_lang)) {
            $this->context->cart->id_lang = $this->context->cookie->id_lang;
        }

        if (is_null($this->context->cart->id_currency)) {
            $this->context->cart->id_currency = $this->context->cookie->id_currency;
        }

        if (is_null($this->context->cart->id_customer)) {
            $this->context->cart->id_customer = $this->context->cookie->id_customer;
        }

        if (is_null($this->context->cart->id_guest)) {
            if (empty($this->context->cookie->id_guest)){
                $this->context->cookie->__set(
                    'id_guest', 
                    Guest::getFromCustomer($this->context->cookie->id_customer)
                );
            }
            $this->context->cart->id_guest = $this->context->cookie->id_guest;
        }

        if (is_null($this->context->cart->id)) {
            $this->context->cart->add();
            $this->context->cookie->__set('id_cart', $this->context->cart->id);
        }

        $algo = new CartChecksum(new AddressChecksum());
        $cart = $this->context->cart;

        $sql_del = "delete from " . _DB_PREFIX_ . "specific_price where id_cart = ".$cart->id." and  id_product = ".$product_id." and id_shop = ".$this->context->shop->id;
        
        Db::getInstance()->execute($sql_del);
        
        $specific_price = new SpecificPrice();
        $specific_price->id_product =$product_id;
        $specific_price->id_cart =$cart->id;
        $specific_price->id_shop =$this->context->shop->id;
        $specific_price->id_currency = 0;
        $specific_price->id_country = 0; 
        $specific_price->id_group = 0;
        $specific_price->id_customer = 0;
        $specific_price->from_quantity = 1;
        $specific_price->price = Tools::getValue('precio');
        $specific_price->reduction_type = 'amount';
        $specific_price->reduction_tax = 1;
        $specific_price->reduction = 0;
        $specific_price->from =date("0000-00-00 00:00:00");// date("Y-m-d H:i:s");
        $specific_price->to = date("Y-m-d").' 23:59:59';
        $specific_price->add();

        $cart->updateQty(1,$product_id);
        
        if ($this->context->customer->isLogged()){
            $sql = 'select id_address from ' . _DB_PREFIX_ . 'address where id_customer = '.$this->context->customer->id.'';
            
            $id_address =  Db::getInstance()->getValue($sql);
            if ($id_address != '' || $id_address != false){
                $cart->id_address_delivery = $id_address;
                $cart->id_address_invoice = $id_address;

                $checkout_session_data='{
                    "checkout-personal-information-step": {
                        "step_is_reachable": true,
                        "step_is_complete": true
                    },
                    "checkout-addresses-step": {
                        "step_is_reachable": true,
                        "step_is_complete": true,
                        "use_same_address": true
                    },
                    "checkout-payment-step": {
                        "step_is_reachable": false,
                        "step_is_complete": false
                    },
                    "checksum": "'.$algo->generateChecksum($cart).'"
                }';
            }
        
        
            Db::getInstance()->execute(
                'UPDATE ' . _DB_PREFIX_ . 'cart SET checkout_session_data = "' . pSQL($checkout_session_data) . '"
                    WHERE id_cart = ' . (int) $cart->id
            );
            return Tools::redirect($this->context->link->getPageLink('order'));
        }
        else
            Tools::redirect('cart?action=show&rrr='.time());
    }
}